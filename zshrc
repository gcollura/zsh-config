# Path to zsh configurations
if [[ -z "$ZSH_ROOT" ]]; then
    export ZSH_ROOT=$HOME/.zsh
fi

# autoload wrapper (zrcautoload) {{{
function zrcautoload() {
    emulate -L zsh
    setopt extended_glob
    local fdir ffile
    local -i ffound

    ffile=$1
    (( ffound = 0 ))
    for fdir in ${fpath} ; do
        [[ -e ${fdir}/${ffile} ]] && (( ffound = 1 ))
    done

    (( ffound == 0 )) && return 1
    if [[ $ZSH_VERSION == 3.1.<6-> || $ZSH_VERSION == <4->* ]] ; then
        autoload -U ${ffile} || return 1
    else
        autoload ${ffile} || return 1
    fi
    return 0
}
# }}}

# Autoloads {{{
zrcautoload zsh-mime-setup && zsh-mime-setup
zrcautoload vcs_info
zrcautoload zmv
zrcautoload zed
zrcautoload chpwd_recent_dirs
zrcautoload cdr
zrcautoload colors && colors
# }}}

# History {{{
HISTFILE=~/.zsh_history         # where to store zsh config
HISTSIZE=1000000                # big history
SAVEHIST=1000000                # big history
setopt append_history           # append
setopt extended_history
setopt hist_ignore_all_dups     # no duplicate
unsetopt hist_ignore_space      # ignore space prefixed commands
setopt hist_reduce_blanks       # trim blanks
setopt hist_verify              # show before executing history commands
setopt inc_append_history       # add commands as they are typed, don't wait until shell exit
setopt share_history            # share hist between sessions
setopt bang_hist                # !keyword
# }}}

# Directory Stack {{{
DIRSTACKSIZE=10                 # limit size of the stack
setopt autopushd                # change behavior of 'cd' to 'pushd'
setopt pushdsilent              # disable messages when push directories
setopt pushdminus               # because - is easier to type than +
setopt pushdignoredups          # ignore dupes
setopt pushdtohome              # pushd behaves like 'pushd $HOME'
# }}}

# Various {{{
setopt auto_cd                  # if command is a path, cd into it
setopt auto_remove_slash        # self explicit
setopt chase_links              # resolve symlinks
setopt correct                  # try to correct spelling of commands
setopt extended_glob            # activate complex pattern globbing
setopt glob_dots                # include dotfiles in globbing
unsetopt print_exit_value         # print return value if non-zero
unsetopt beep                   # no bell on error
unsetopt bg_nice                # no lower prio for background jobs
unsetopt clobber                # must use >| to truncate existing files
unsetopt hist_beep              # no bell on error in history
unsetopt hup                    # no hup signal at shell exit
unsetopt ignore_eof             # do not exit on end-of-file
unsetopt list_beep              # no bell on ambiguous completion
unsetopt rm_star_silent         # ask for confirmation for `rm *' or `rm path/*'
# }}}

# Zsh hooks {{{
function precmd {
    print -Pn '\e]0;%M:%~\a'    # terminal title: hostname:~/path/to/dir
    vcs_info
}

function chpwd {
    chpwd_recent_dirs
}
# }}}

# Command not found {{{
# Uses the command-not-found package zsh support as seen in
# http://www.porcheron.info/command-not-found-for-zsh/ this is installed in
# Ubuntu
[[ -e /etc/zsh_command_not_found ]] && source /etc/zsh_command_not_found
# Arch Linux command-not-found support, you must have package pkgfile installed
# https://wiki.archlinux.org/index.php/Pkgfile#.22Command_not_found.22_hook
[[ -e /usr/share/doc/pkgfile/command-not-found.zsh ]] && source /usr/share/doc/pkgfile/command-not-found.zsh
# }}}

# Functions {{{
function take() {
    if [[ -n $1 && -d $1 ]]; then
        builtin cd $1
    elif [[ -n $1 ]]; then
        mkdir -p $1 && builtin cd $1
    fi
}

function ssh-byobu() {
    command ssh -At $@ byobu
}

function ssh-tmux() {
    command ssh -At $@ tmux
}

# Tmux
function trw() {
    if [[ -z $TMUX ]]; then
        return
    fi
    local window=`basename $PWD`
    if [[ -n $1 ]]; then
        window="$1"
    fi
    command tmux rename-window "$window"
}

function sensors() {
    # alias sensors='sensors && aticonfig --od-gettemperature'
    if type sensors > /dev/null ; then
        command sensors
    fi
    if type aticonfig > /dev/null ; then
        command aticonfig --od-gettemperature
    fi
}

function cd() {
    # cd /etc/fstab
    if [[ -f ${1} ]]; then
        [[ ! -e ${1:h} ]] && return 1
        print "Correcting ${1} to ${1:h}"
        builtin cd ${1:h}
    else
        builtin cd ${1}
    fi
}

function vcs_info_wrapper() {
    if [ -n "$vcs_info_msg_0_" ]; then
        echo "%{$fg[red]%}${vcs_info_msg_0_%% }%{$reset_color%}$del "
    fi
}

### git: Show marker ? if there are untracked files in repository
# Make sure you have added staged to your 'formats':  %c
function +vi-git-untracked() {
    if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
        git status --porcelain | fgrep '??' &> /dev/null ; then
        # This will show the marker if there are any untracked files in repo.
        # If instead you want to show the marker only if there are untracked
        # files in $PWD, use:
        #[[ -n $(git ls-files --others --exclude-standard) ]] ; then
        hook_com[unstaged]+='%B%F{magenta}?%f%b'
    fi
}

### git: Show +N/-N when your local branch is ahead-of or behind remote HEAD.
# Make sure you have added misc to your 'formats':  %m
function +vi-git-aheadbehind() {
    local ahead behind
    local -a gitstatus

    # for git prior to 1.7
    # ahead=$(git rev-list origin/${hook_com[branch]}..HEAD | wc -l)
    ahead=$(git rev-list ${hook_com[branch]}@{upstream}..HEAD 2>/dev/null | wc -l | tr -d ' ')
    (( $ahead )) && gitstatus+=( "%B%F{green}+${ahead}%f%b" )

    # for git prior to 1.7
    # behind=$(git rev-list HEAD..origin/${hook_com[branch]} | wc -l)
    behind=$(git rev-list HEAD..${hook_com[branch]}@{upstream} 2>/dev/null | wc -l | tr -d ' ')
    (( $behind )) && gitstatus+=( "%B%F{red}-${behind}%f%b" )

    hook_com[misc]+=${(j::)gitstatus}
}

function pathmunge() {
    # From https://unix.stackexchange.com/a/217629
    # Don't add to PATH if it doesn't exist.
    if [ ! -d $1 ]; then
        return
    fi
    if ! echo "$PATH" | /bin/grep -Eq "(^|:)$1($|:)" ; then
        if [ "$2" = "after" ] ; then
            PATH="$PATH:$1"
        else
            PATH="$1:$PATH"
        fi
    fi
}
# }}}

# Editor {{{
if type nvim > /dev/null; then
    export EDITOR='nvim'
else
    export EDITOR='vim'
fi
# }}}

# Android {{{
export ANDROID_HOME=$HOME/.android/sdk
export ANDROID_SDK_ROOT=$HOME/.android/sdk
export ANDROID_NDK=$HOME/.android/ndk
export ANDROID_NDK_ROOT=$HOME/.android/ndk
export ANDROID_NDK_HOST=linux-x86_64

if [ -d $ANDROID_HOME ]; then
    pathmunge $ANDROID_HOME/tools
    pathmunge $ANDROID_HOME/tools/bin
    pathmunge $ANDROID_HOME/platform-tools
fi
# }}}

# Java {{{
export ECLIPSE_HOME=/opt/eclipse
# }}}

# Go {{{
export GOPATH=$HOME/Code/go
pathmunge $GOPATH/bin
# }}}

# Dircolors {{{
DIRCOLORS=$HOME/.dircolors
if [ -f $DIRCOLORS ] && type dircolors > /dev/null ; then
    # eval $(dircolors -b $DIRCOLORS)
fi
# }}}

# Completion {{{
zrcautoload compinit && compinit
zmodload -i zsh/complist
setopt menu_complete            # select the first entry
unsetopt flowcontrol
setopt alwayslastprompt
setopt hash_list_all            # hash everything before completion
setopt completealiases          # complete alisases
setopt always_to_end            # when completing from the middle of a word, move the cursor to the end of the word
setopt list_types
setopt complete_in_word         # allow completion from within a word/phrase
setopt correct                  # spelling correction for commands
setopt list_ambiguous           # complete as much of a completion until it gets ambiguous.
setopt cdablevars

zstyle ':completion::complete:*' use-cache on               # completion caching, use rehash to clear
zstyle ':completion::complete:*' rehash true
zstyle ':completion:*' cache-path ~/.zsh/cache              # cache path
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'   # ignore case
zstyle ':completion:*' menu select=2                        # menu if nb items > 2
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}       # colorz !
zstyle ':completion:*::::' completer _expand _complete _ignored _approximate # list of completers to use
zstyle ':completion:*:functions' ignored-patterns '_*'

zstyle ':completion:*' verbose yes
zstyle ':completion:*' format '%B---- %d%b'
zstyle ':completion:*' group-name ''
zstyle ':completion:*:descriptions' format $'\e[00;34m%d'
zstyle ':completion:*:messages' format $'\e[00;31m%d ---'
zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*:warnings' format "$fg[red]No matches for:$reset_color %d"

zstyle ':completion:*:processes' command 'ps -au$USER'
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*' force-list always
zstyle ':completion:*:*:kill:*:processes' list-colors "=(#b) #([0-9]#)*=29=34"
zstyle ':completion:*:*:killall:*' menu yes select
zstyle ':completion:*:killall:*' force-list always
users=(gcoll random root)
zstyle ':completion:*' users $users

zstyle ':completion:*:*:task:*' verbose yes
zstyle ':completion:*:*:task:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:*:task:*' group-name ''

# Make zsh know about hosts already accessed by SSH
zstyle -e ':completion:*:(ssh|scp|sftp|rsh|rsync):hosts' hosts 'reply=(${=${${(f)"$(cat {/etc/ssh_,~/.ssh/known_}hosts(|2)(N) /dev/null)"}%%[# ]*}//,/ })'

# ignore duplicate entries
zstyle ':completion:*:history-words' remove-all-dups yes
zstyle ':completion:*:history-words' stop yes

# completion for cdr
zstyle ':completion:*:*:cdr:*:*' menu selection
# }}}

# Git {{{
zstyle ':vcs_info:*' enable git cvs svn
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' unstagedstr '%B%F{red}!%b%f'
zstyle ':vcs_info:*' stagedstr '%B%F{green}+%b%f'
zstyle ':vcs_info:*' actionformats '%s:%b (%a) %m%u%c'
zstyle ':vcs_info:*' formats '%s:%b %m%u%c'
zstyle ':vcs_info:git:*' patch-format '%7>>%p%<< (%n applied)'
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked git-aheadbehind
# }}}

# Plugins {{{
local ZGEN_ROOT="$ZSH_ROOT/zgen"
if [ ! -d $ZGEN_ROOT ]; then
    command git clone https://github.com/tarjoilija/zgen.git $ZGEN_ROOT
fi
source $ZGEN_ROOT/zgen.zsh

if ! zgen saved ; then
    zgen oh-my-zsh plugins/tmux

    zgen load zsh-users/zsh-syntax-highlighting
    zgen load zsh-users/zsh-history-substring-search
    zgen load zsh-users/zsh-completions src

    zgen save
fi
# }}}

# Key bindings {{{
bindkey -v

if [[ "${terminfo[khome]}" != "" ]]; then
    bindkey "${terminfo[khome]}" beginning-of-line # [Home] - Go to beginning of line
fi

if [[ "${terminfo[kend]}" != "" ]]; then
    bindkey "${terminfo[kend]}" end-of-line # [End] - Go to end of line
fi

if [[ "${terminfo[kcbt]}" != "" ]]; then
    bindkey "${terminfo[kcbt]}" reverse-menu-complete # [Shift-Tab] - move through the completion menu backwards
fi

bindkey ' ' magic-space # [Space] - do history expansion
bindkey '^[[1;5C' forward-word # [Ctrl-RightArrow] - move forward one word
bindkey '^[[1;5D' backward-word # [Ctrl-LeftArrow] - move backward one word

# Make vi mode behave sanely
bindkey '^?' backward-delete-char
bindkey '^W' backward-kill-word
bindkey '^H' backward-delete-char      # Control-h also deletes the previous char
bindkey '^U' backward-kill-line

bindkey '^R' history-incremental-search-backward
# }}}

# Prompt {{{
setopt prompt_subst # Allow for functions in the prompt
local ret_status="%(?:%{$fg[white]%}:%{$fg[red]%})➤ %{$reset_color%}"
PROMPT='%{$fg[blue]%}%3~%{$reset_color%} $(vcs_info_wrapper)${ret_status}'
# }}}

# Aliases {{{
# List direcory contents
ls --color -d . &>/dev/null 2>&1 && alias ls='ls --color=tty --group-directories-first' || alias ls='ls -G'
alias lsa='ls -lah'
alias l='ls -lah'
alias ll='ls -lh'
alias la='ls -lAh'

alias -g ...='../../'
alias -g ....='../../../'

# Directory Stack
alias dirs='dirs -v'
alias pd='popd'

# be more human
alias df='df -h'
alias free='free -h'

alias zshrc="$EDITOR ~/.zshrc" # Quick access to the ~/.zshrc file
alias zshreload="source ~/.zshrc"

if type tmux2 > /dev/null ; then
    alias tmux=tmux2
    alias tm="agenttmux2 new-session -A -s work"
fi

# Show progress while file is copying
# Rsync options are:
# -p - preserve permissions
# -o - preserve owner
# -g - preserve group
# -h - output in human-readable format
# --progress - display progress
# -b - instead of just overwriting an existing file, save the original
# --backup-dir=/tmp/rsync - move backup copies to "/tmp/rsync"
# -e /dev/null - only work on local files
# -- - everything after this is an argument, even if it looks like an option
alias cpv="rsync -poghb --backup-dir=/tmp/rsync -e /dev/null --progress --"

# Add an "alert" alias for long running commands. Use like so:
# $ sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Programming
alias build='mkdir build ; cd build && cmake .. && make ; cd .. && ls'

# System info
alias pg='ps aux | grep'  # requires an argument
# alias sensors='sensors && aticonfig --od-gettemperature'

# Misc
alias :q="exit"
alias e="$EDITOR"
alias ccat='pygmentize -O bg=dark'

# Git
alias glog='git log --graph --full-history --all --color --pretty=format:"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%s"'
alias git-clean-all='git clean -fXd'
alias g='git'
compdef g='git'

# PHP
alias xdebug-on='sudo php5enmod -s cli xdebug'
alias xdebug-off='sudo php5dismod -s cli xdebug'

# directory shortcut ~ named directories
hash -d projects="$HOME/Projects"
hash -d university="$HOME/Projects/university"
hash -d zero11="$HOME/Projects/zero11"
hash -d desktop="$HOME/Desktop"
hash -d downloads="$HOME/Downloads"
hash -d eurecom="$HOME/Projects/eurecom"
if [ -d "$HOME/ownCloud" ]; then
    hash -d owncloud="$HOME/ownCloud"
fi
if [ -d "$HOME/Nextcloud" ]; then
    hash -d nextcloud="$HOME/Nextcloud"
    hash -d owncloud="$HOME/Nextcloud"
fi
if [ -d "$HOME/Code" ]; then
    if [ ! -d "$HOME/Projects" ]; then
        hash -d projects="$HOME/Code"
    fi
    hash -d code="$HOME/Code"
fi
if [ -d $GOPATH ]; then
    hash -d go="$GOPATH"
fi

# Docker {{{
if type docker > /dev/null ; then
    alias sen='docker run --rm --privileged -v /var/run/docker.sock:/run/docker.sock -it -e TERM tomastomecek/sen'
    alias docker-update-images='docker images --format "{{.Repository}}" | xargs -L1 docker pull'
    alias docker-dangling-images='docker images --filter "dangling=true"'
    alias docker-remove-dangling-images='docker rmi $(docker images -f "dangling=true" -q)'
    alias docker-clean-volumes='docker volume ls -qf dangling=true | xargs -r docker volume rm'
    alias docker-clean-containers='docker ps --filter status=dead --filter status=exited -aq | xargs -r docker rm -v'
fi
# }}}

alias py='python'
alias py3='python3'
alias notebook='jupyter notebook'
# }}}

# Cursor {{{
zle -N zle-keymap-select
zle -N zle-line-init
zle -N zle-line-finish

function zle-keymap-select {
    # change cursor shape in urxvt
    case $KEYMAP in
        vicmd)      print -n -- "\033[2 q";;  # block cursor
        viins|main) print -n -- "\033[5 q";;  # line blinking cursor
    esac
    zle -R
}

function zle-line-init {
    print -n -- "\033[5 q"
}

function zle-line-finish {
    print -n -- "\033[2 q"  # block cursor
}
# }}}

# fzf {{{
function fzf-install() {
    command git clone --depth 1 https://github.com/junegunn/fzf.git $ZSH_ROOT/fzf
    $ZSH_ROOT/fzf/install --completion --key-bindings --no-update-rc --no-fish
}

function fzf-update() {
    builtin cd $ZSH_ROOT/fzf
    command git pull
    $ZSH_ROOT/fzf/install --completion --key-bindings --no-update-rc --no-fish
}

if [ -n $TMUX ]; then
    export FZF_TMUX=1
fi

[ -f $HOME/.fzf.zsh ] && source $HOME/.fzf.zsh
# }}}

pathmunge $HOME/.local/bin
pathmunge $HOME/.bin
pathmunge /snap/bin after

# vim: fdm=marker et fen fdl=0 tw=0
